# Feature Toggles


## Summary
First of all, I ran out of time. I got stuck on the example feature API post request. To be more precise - with DB data seeding for testing (defining many-to-many relations for the test data). I should had gone with an SQL script which would fill the DB tables. To be fair, I should had probably gone with the frontend first and to support it with API which then would return mostly only hard-coded data.

Secondly, the project consists of two sub-projects, the backend and the frontend. Backend is built on .Net Core 3.1 and Angular was used for the frontend.

### Planned tasks (in a scenario where I do not run out of time... :) )
	1. DB filling with test data for further work
	2. API CRUD for featureToggles (currently it is only a "dotnet aspnet-codegenerator" generated file)
	3. Angular Material webform (currently frontend has only the default Angular project page)
	4. Webform data validation
	5. Webform interaction with the API
	6. Extensive testing until the time runs out

## Requirements
	* .Net Core 3.1
	* Angular (11.2+)
    * SQL Server (connection string can be found in "FeatureTogglesApi/Models/FeatureTogglesContext")
	
## Assumptions:
	- Feature API request points only to feature names (and to no any other feature parameters).
	- In this feature API request "name" stands for "technical name" (not "display name").
	- It is not in the scope of this particular app in this particular version to manage the feature and client lists (adding, removing/archiving new entries), only the Feature Toggle entries.

## Hypothetical to-do list after the planned tasks
	* Deeper logic analysis on the API response in the given example, perhaps extra data and reading.
	* Merging Angular and .NET Core projects into a single project (I remembered that this can be done in the middle of the exercise, already running out of the planned time).
	* DB connection string for API in secrets.
	* Moving API logic out of the Controller files.
	* Setting file for things such as API version and the API URL for the front-end. 
	* Automated unit tests.
	* API logging.
	* Possibly extra input validation in the API.
	* Docker file.
	* Authentification & registration & route guards.



