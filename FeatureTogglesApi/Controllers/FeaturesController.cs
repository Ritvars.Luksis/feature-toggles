using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FeatureTogglesApi.DTO;
using FeatureTogglesApi.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FeatureTogglesApi.Controllers {
    [Route ("api/v1/[controller]")]
    [ApiController]
    public class FeaturesController : ControllerBase {
        private readonly FeatureTogglesContext _context;

        public FeaturesController (FeatureTogglesContext context) {
            _context = context;
        }

        // POST: api/v1/features
        [HttpPost]
        public ActionResult<List<Feature>> GetFeatures (FeatureRequest featureRequest) {
            List<string> featureNames = featureRequest.Features.Select (a => a.Name).ToList<string> ();
            List<Feature> featuresResult = new List<Feature> ();

            foreach (string featureName in featureNames) {
                Feature feature = new Feature ();

                feature.Name = featureName;
                feature.Active = active (featureName, getClientFeatureToggles(featureRequest));
                feature.Inverted = inverted (featureName);
                feature.Expired = expired (featureName);

                featuresResult.Add (feature);
            }

            return CreatedAtAction ("GetFeaturesAsync", new { id = featureRequest.CustomerId }, featuresResult);
        }

        private List<Models.FeatureToggle> getClientFeatureToggles (FeatureRequest featureRequest) {
            List<string> featureNames = featureRequest.Features.Select (a => a.Name).ToList<string> ();

            List<Models.FeatureToggle> clientFeatureToggles = _context.FeatureToggles
                .Where (fn => featureNames.Contains (fn.DisplayName) && fn.Customers.Select (a => a.Id).Contains (featureRequest.CustomerId))
                .ToList<Models.FeatureToggle> ();

            return clientFeatureToggles;
        }

        private bool active (string featureName, List<Models.FeatureToggle> clientFeatureToggles) {
            bool onList, inverted, expired, active = false;

            Models.FeatureToggle featureToggle = getFeatureToggle (featureName);

            onList = clientFeatureToggles.FirstOrDefault (a => a.TechnicalName == featureName) != null;
            inverted = featureToggle.Inverted;
            expired = this.expired (featureName);

            // TODO: test and probably upgrade the following logic
            active = onList;
            active = inverted ? !active : active;
            active = expired ? !active : active;

            return active;
        }

        private bool inverted (string featureName) {
            Models.FeatureToggle featureToggle = getFeatureToggle (featureName);

            if (featureToggle != null) {

                return featureToggle.Inverted;
            }

            return false;
        }

        private bool expired (string featureName) {
            Models.FeatureToggle featureToggle = getFeatureToggle (featureName);

            if (featureToggle != null) {

                return featureToggle.ExpiresOn < DateTime.UtcNow;
            }

            return false;
        }

        private Models.FeatureToggle getFeatureToggle (string featureName) {
            return _context.FeatureToggles.FirstOrDefault (a => a.TechnicalName == featureName);
        }
    }
}