using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FeatureTogglesApi.Models;

namespace FeatureTogglesApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FeatureTogglesController : ControllerBase
    {
        private readonly FeatureTogglesContext _context;

        public FeatureTogglesController(FeatureTogglesContext context)
        {
            _context = context;
        }

        // GET: api/FeatureToggles
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FeatureToggle>>> GetFeatureToggles()
        {
            return await _context.FeatureToggles.ToListAsync();
        }

        // GET: api/FeatureToggles/5
        [HttpGet("{id}")]
        public async Task<ActionResult<FeatureToggle>> GetFeatureToggle(long id)
        {
            var featureToggle = await _context.FeatureToggles.FindAsync(id);

            if (featureToggle == null)
            {
                return NotFound();
            }

            return featureToggle;
        }

        // PUT: api/FeatureToggles/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFeatureToggle(long id, FeatureToggle featureToggle)
        {
            if (id != featureToggle.Id)
            {
                return BadRequest();
            }

            _context.Entry(featureToggle).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FeatureToggleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/FeatureToggles
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<FeatureToggle>> PostFeatureToggle(FeatureToggle featureToggle)
        {
            _context.FeatureToggles.Add(featureToggle);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFeatureToggle", new { id = featureToggle.Id }, featureToggle);
        }

        // DELETE: api/FeatureToggles/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<FeatureToggle>> DeleteFeatureToggle(long id)
        {
            var featureToggle = await _context.FeatureToggles.FindAsync(id);
            if (featureToggle == null)
            {
                return NotFound();
            }

            _context.FeatureToggles.Remove(featureToggle);
            await _context.SaveChangesAsync();

            return featureToggle;
        }

        private bool FeatureToggleExists(long id)
        {
            return _context.FeatureToggles.Any(e => e.Id == id);
        }
    }
}
