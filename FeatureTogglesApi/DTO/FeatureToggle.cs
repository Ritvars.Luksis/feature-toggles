using System;
using System.Collections.Generic;

namespace FeatureTogglesApi.DTO
{
    public class FeatureToggle
    {
        public string DisplayName { get; set; }
        public string TechnicalName { get; set; }
        public DateTime ExpiresOn { get; set; }
        public string Description { get; set; }
        public bool Inverted { get; set; }
        public List<string> CustomerIds { get; set; }
    }
}