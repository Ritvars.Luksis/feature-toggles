using System.Collections.Generic;

namespace FeatureTogglesApi.DTO
{
    public class FeatureRequest
    {
        public string CustomerId { get; set; }
        public List<FeatureRequestFeature> Features { get; set; }
    }

    public class FeatureRequestFeature {
        public string Name { get; set; }
    }
}