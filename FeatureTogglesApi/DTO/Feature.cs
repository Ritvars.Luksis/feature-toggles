namespace FeatureTogglesApi.DTO
{
    public class Feature
    {
        public string Name { get; set; }
        public bool Active { get; set; }
        public bool Inverted { get; set; }
        public bool Expired { get; set; }
    }
}