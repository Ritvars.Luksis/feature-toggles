﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace FeatureTogglesApi.Migrations
{
    [DbContext(typeof(FeatureTogglesContext))]
    [Migration("20210725114016_RemoveFeatureModel")]
    partial class RemoveFeatureModel
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.8")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("CustomerFeatureToggle", b =>
                {
                    b.Property<string>("CustomersId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<long>("FeatureTogglesId")
                        .HasColumnType("bigint");

                    b.HasKey("CustomersId", "FeatureTogglesId");

                    b.HasIndex("FeatureTogglesId");

                    b.ToTable("CustomerFeatureToggle");
                });

            modelBuilder.Entity("FeatureTogglesApi.Models.Customer", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Customers");
                });

            modelBuilder.Entity("FeatureTogglesApi.Models.FeatureToggle", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("DisplayName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("ExpiresOn")
                        .HasColumnType("datetime2");

                    b.Property<bool>("Inverted")
                        .HasColumnType("bit");

                    b.Property<string>("TechnicalName")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("FeatureToggles");
                });

            modelBuilder.Entity("CustomerFeatureToggle", b =>
                {
                    b.HasOne("FeatureTogglesApi.Models.Customer", null)
                        .WithMany()
                        .HasForeignKey("CustomersId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("FeatureTogglesApi.Models.FeatureToggle", null)
                        .WithMany()
                        .HasForeignKey("FeatureTogglesId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
