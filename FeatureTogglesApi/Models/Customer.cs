using System.Collections.Generic;

namespace FeatureTogglesApi.Models {
    public class Customer {
        public string Id { get; set; }
        public string Name { get; set; }
        public ICollection<FeatureToggle> FeatureToggles { get; set; }
    }
}