using System;
using System.Collections.Generic;

namespace FeatureTogglesApi.Models
{
    public class FeatureToggle
    {
        public long Id { get; set; }
        public string DisplayName { get; set; }
        public string TechnicalName { get; set; }
        public DateTime ExpiresOn { get; set; }
        public string Description { get; set; }
        public bool Inverted { get; set; }
        public ICollection<Customer> Customers {get; set; }
    }
}