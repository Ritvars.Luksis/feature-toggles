using FeatureTogglesApi.Models;
using Microsoft.EntityFrameworkCore;

public class FeatureTogglesContext : DbContext
{
    public DbSet<FeatureToggle> FeatureToggles { get; set; }
    public DbSet<Customer> Customers { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlServer(@"Server=localhost\SQLEXPRESS;Database=master;Trusted_Connection=True;");
    }
}